package com.example.macbook.diploma.animation;

import android.view.View;
import android.view.animation.Animation;

/**
 * Created by macbook on 18.01.16.
 */
public abstract class ViewAnimationListener implements Animation.AnimationListener {

    private final View view;

    protected ViewAnimationListener(View view) {
        this.view = view;
    }

    @Override
    public void onAnimationStart(Animation animation) {
        onAnimationStart(this.view, animation);
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        onAnimationEnd(this.view, animation);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    protected abstract void onAnimationStart(View view, Animation animation);
    protected abstract void onAnimationEnd(View view, Animation animation);
}
