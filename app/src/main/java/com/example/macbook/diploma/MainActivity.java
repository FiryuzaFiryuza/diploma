package com.example.macbook.diploma;

import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.macbook.diploma.adapters.RecyclerViewAdapter;
import com.example.macbook.diploma.dataBase.DataBase;
import com.example.macbook.diploma.models.Lesson;
import com.example.macbook.diploma.services.DBService;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends BaseActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView mTextNoLessons;
    private FloatingActionButton mBttnAdd;
    private Button mBttnDelete;
    private Button mBttnCancel;
    private View mSwipeBg;
    private boolean isCanceled;
    private boolean isDeleted;
    private boolean exit;

    private DBService dbService = new DBService(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolbar();
        initControls();

        final ArrayList<Lesson> lessons = dbService.getLessons();

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new RecyclerViewAdapter(lessons, this);
        mRecyclerView.setAdapter(mAdapter);
        final ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            }


            @Override
            public void onChildDraw(final Canvas c, final RecyclerView recyclerView, final RecyclerView.ViewHolder viewHolder, final float dX, final float dY, final int actionState, final boolean isCurrentlyActive) {
                final View itemView = viewHolder.itemView;
                mSwipeBg.setY(itemView.getTop());
                if(isCurrentlyActive) {
                    mSwipeBg.setVisibility(View.INVISIBLE);
                }else{
                    mSwipeBg.setVisibility(View.VISIBLE);
                    mBttnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    private void initControls() {
        mSwipeBg = findViewById(R.id.fl_swipe_left);

        mBttnAdd = (FloatingActionButton) findViewById(R.id.bttn_lesson_add);
        mBttnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LessonDetails.class);
                intent.putExtra("lesson", -1);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(intent);
            }
        });

        mBttnDelete = (Button) findViewById(R.id.bttn_delete_card);
        mBttnCancel = (Button) findViewById(R.id.bttn_cancel_card);
        mBttnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSwipeBg.setVisibility(View.GONE);
            }
        });
    }

    public void onDismiss(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, int position) {
        //TODO delete the actual item in your data source
        mAdapter.notifyItemRemoved(position);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
}
