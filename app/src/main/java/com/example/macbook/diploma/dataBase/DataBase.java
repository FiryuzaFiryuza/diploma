package com.example.macbook.diploma.dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.macbook.diploma.models.Lesson;
import com.example.macbook.diploma.models.Program;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by macbook on 01.12.15.
 */
public class DataBase {
    private DataBaseHelper dataBase;
    private SQLiteDatabase sqLiteDatabase;

    public DataBase(Context context) throws IOException {
        this.dataBase = new DataBaseHelper(context);
        this.dataBase.createDataBase();
        this.open();
    }

    public void close() {
        this.dataBase.close();
        this.sqLiteDatabase.close();
    }

    public ArrayList<Lesson> getLessons() {
        ArrayList<Lesson> lessonsList = new ArrayList<Lesson>();

        String selectQuery = "SELECT * FROM " + DBFields.TABLE_LESSON;

        Cursor cursor = this.sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Lesson lesson = new Lesson();
                lesson.setId(cursor.getInt(cursor.getColumnIndex(DBFields.KEY_ID)));
                lesson.setName(cursor.getString(cursor.getColumnIndex(DBFields.KEY_NAME)));
                lesson.setDescription(cursor.getString(cursor.getColumnIndex(DBFields.KEY_DESCRIPTION)));
                lesson.setDuration(cursor.getDouble(cursor.getColumnIndex(DBFields.KEY_DURATION)));
                lesson.setIsSystemLesson(cursor.getInt(cursor.getColumnIndex(DBFields.KEY_IS_SYSTEM_LESSON)) == 1);
                lessonsList.add(lesson);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return lessonsList;
    }

    public Lesson getLessonById(long id) {
        Cursor cursor = this.sqLiteDatabase.query(DBFields.TABLE_LESSON, new String[] { DBFields.KEY_ID,
                        DBFields.KEY_NAME, DBFields.KEY_DESCRIPTION, DBFields.KEY_DURATION, DBFields.KEY_IS_SYSTEM_LESSON}, DBFields.KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Lesson lesson = new Lesson(cursor.getLong(0),
                cursor.getString(1), cursor.getString(2), cursor.getDouble(3), cursor.getInt(4) == 1);

        return lesson;
    }

    public ArrayList<Program> getProgramsFromLesson(long id) {
        ArrayList<Program>  programsList = new ArrayList<Program>();
        Program program;
        String selectQuery;
        Cursor cursor;

        /*
        String selectQuery = "SELECT "+ TourDataBase.TABLE_SIGHTS + "." + TourDataBase.KEY_NAME +
                ", " + TourDataBase.TABLE_SIGHTS + "." + TourDataBase.KEY_CATEGORIES_ID +
                " From " + TourDataBase.TABLE_SIGHTS_FROM_TOUR +
                " INNER JOIN " + TourDataBase.TABLE_READY_TOUR + " ON " +
                TourDataBase.TABLE_SIGHTS_FROM_TOUR + "." + TourDataBase.KEY_TOUR_ID + "=" +
                TourDataBase.TABLE_READY_TOUR + "." + TourDataBase.KEY_ID +
                " INNER JOIN " + TourDataBase.TABLE_SIGHTS + " ON " +
                TourDataBase.TABLE_SIGHTS_FROM_TOUR + "." + TourDataBase.KEY_SIGHTS_ID + "=" +
                TourDataBase.TABLE_SIGHTS + "." + TourDataBase.KEY_ID +
                " WHERE " + TourDataBase.KEY_TOUR_ID + "=" + id;
        */

        selectQuery = "SELECT * " +
                " FROM " + DBFields.TABLE_PROGRAM +
                " WHERE " + DBFields.KEY_LESSON_ID + "=?";
        cursor = this.sqLiteDatabase.rawQuery(selectQuery, new String[] { String.valueOf(id) });

        if (cursor.moveToFirst()) {
            do {

                program = new Program(cursor.getLong(0),
                        cursor.getString(1), cursor.getDouble(2),
                        cursor.getInt(3)  == 1, cursor.getDouble(4),
                        cursor.getDouble(5), cursor.getDouble(6),
                        cursor.getDouble(7), cursor.getLong(8));

                programsList.add(program);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return programsList;
    }

    public Program getProgramById(long id) {
        Cursor cursor = this.sqLiteDatabase.query(DBFields.TABLE_PROGRAM, new String[] { DBFields.KEY_ID,
                        DBFields.KEY_NAME, DBFields.KEY_DURATION,
                        DBFields.KEY_BREATH, DBFields.KEY_OUT,
                        DBFields.KEY_HOLD_FIRST, DBFields.KEY_HOLD_SECOND,
                        DBFields.KEY_UNIT, DBFields.KEY_LESSON_ID }, DBFields.KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Program program = new Program(cursor.getLong(0),
                cursor.getString(1), cursor.getDouble(2),
                cursor.getDouble(3), cursor.getDouble(4),
                cursor.getDouble(5),cursor.getDouble(6),
                cursor.getInt(7)  == 1, cursor.getLong(8));

        return program;
    }

    public void addProgram(Program program) {
        int unit = program.getUnit() ? 1 : 0;

        String selectQuery = "INSERT INTO " + DBFields.TABLE_PROGRAM + " (" +
                DBFields.KEY_LESSON_ID + ", " + DBFields.KEY_NAME + "," +
                DBFields.KEY_DURATION + "," + DBFields.KEY_UNIT + "," +
                DBFields.KEY_BREATH + "," + DBFields.KEY_OUT + "," +
                DBFields.KEY_HOLD_FIRST + "," + DBFields.KEY_HOLD_SECOND + ") values (" +
                program.getLessonId() + ", " + "\"" + program.getName() + "\"" + "," +
                program.getDuration() + "," + unit + "," +
                program.getBreath() + "," + program.getOut() + "," +
                program.getHoldFirst() + "," + program.getHoldSecond() + ");";

        this.sqLiteDatabase.execSQL(selectQuery);
    }

    public void updateProgram(Program program) {
        ContentValues values = new ContentValues();
        int unit = program.getUnit() ? 1 : 0;

        values.put(DBFields.KEY_NAME, program.getName());
        values.put(DBFields.KEY_DURATION, program.getDuration());
        values.put(DBFields.KEY_UNIT, unit);
        values.put(DBFields.KEY_BREATH, program.getBreath());
        values.put(DBFields.KEY_OUT, program.getOut());
        values.put(DBFields.KEY_HOLD_FIRST, program.getHoldFirst());
        values.put(DBFields.KEY_HOLD_SECOND, program.getHoldSecond());

        sqLiteDatabase.update(DBFields.TABLE_PROGRAM, values, "_id " + "=" + program.getId(), null);
    }

    public long addLesson(Lesson lesson) {
        ContentValues values = new ContentValues();

        values.put(DBFields.KEY_NAME, lesson.getName());
        values.put(DBFields.KEY_DESCRIPTION, lesson.getDescription());
        values.put(DBFields.KEY_IS_SYSTEM_LESSON, 0);

        sqLiteDatabase.insert(DBFields.TABLE_LESSON, null, values);

        return getLessonIdByMetadat(lesson.getName(), lesson.getDescription());
    }

    public void updateLesson(Lesson lesson) {
        ContentValues values = new ContentValues();

        values.put(DBFields.KEY_NAME, lesson.getName());
        values.put(DBFields.KEY_DESCRIPTION, lesson.getDescription());

        sqLiteDatabase.update(DBFields.TABLE_LESSON, values, "_id " + "=" + lesson.getId(), null);
    }

    private long getLessonIdByMetadat(String name, String description) {
        String selectQuery = "SELECT _id " +
                " FROM " + DBFields.TABLE_LESSON +
                " WHERE " + DBFields.KEY_NAME + "=?" + "AND " + DBFields.KEY_DESCRIPTION + "=?";
        Cursor cursor = this.sqLiteDatabase.rawQuery(selectQuery, new String[] {name, description });
        if (cursor != null)
            cursor.moveToFirst();

       return cursor.getLong(0);
    }


    private void open() throws SQLException {
        sqLiteDatabase = dataBase.getWritableDatabase();
    }
}
