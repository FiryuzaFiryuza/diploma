package com.example.macbook.diploma.dataBase;

/**
 * Created by macbook on 01.12.15.
 */
public class DBFields {
    // tables in db
    public static final String TABLE_LESSON = "lesson";
    public static final String TABLE_PROGRAM = "program";

    // common fileds in tables
    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_DURATION = "duration";

    // other fileds of all tables
    public static final String KEY_IS_SYSTEM_LESSON = "is_system_lesson";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_BREATH = "breath";
    public static final String KEY_OUT = "out";
    public static final String KEY_HOLD_FIRST= "hold_first";
    public static final String KEY_HOLD_SECOND= "hold_second";
    public static final String KEY_UNIT= "unit";
    public static final String KEY_LESSON_ID = "lesson_id";
}
