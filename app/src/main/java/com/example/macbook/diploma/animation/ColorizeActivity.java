package com.example.macbook.diploma.animation;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.CycleInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;

import com.example.macbook.diploma.LessonDetails;
import com.example.macbook.diploma.MainActivity;
import com.example.macbook.diploma.R;
import com.example.macbook.diploma.dataBase.DataBase;
import com.example.macbook.diploma.models.NameValuePair;
import com.example.macbook.diploma.models.Program;
import com.example.macbook.diploma.services.DBService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by macbook on 23.11.15.
 */
public class ColorizeActivity extends AppCompatActivity implements SurfaceHolder.Callback, NumberPicker.OnValueChangeListener {
    private long lessonId;
    private ArrayList<Program> programList;

    private SurfaceHolder mSurfaceHolder;
    private SurfaceView mSurfaceView;

    private Paint mPaint;
    private RectF mOval;
    private Paint mArcPaint;
    private float mAngle;
    private float mPartAngle;
    private Timer timer;
    private double programDuration;
    private double curDuration;
    private int count;
    private int startTimer;
    private String startPosition;
    private String curPosition = "Вдох";
    private int curItem = 0;

    private int sizeOfPrograms;
    private double lessonDuration;

    private FloatingActionButton mBttnControl;
    private ImageView mBttnClose;
    private ImageView mBttnNext;
    private ImageView mBttnBack;
    private ImageView heartImage;
    Animation pulse;

    private boolean isStopped = false;
    private boolean hasCardio = false;
    private float userPulse = 80;

    private int heightScreen;
    private int widthScreen;

    private DBService dbService = new DBService(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        heightScreen = metrics.heightPixels;
        widthScreen = metrics.widthPixels;

        initializeControls();

        Bundle extra = getIntent().getBundleExtra("start");
        ArrayList<NameValuePair> objects = (ArrayList<NameValuePair>) extra.getSerializable("objects");

        //Bundle id = getIntent().getExtras();
        lessonId = Long.parseLong(objects.get(0).getValue());
        userPulse = Float.parseFloat(objects.get(1).getValue());

        programList = dbService.getProgramsFromLesson(lessonId);
        sizeOfPrograms = programList.size();

        if(sizeOfPrograms == 0) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("noLessons", -1);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return;
        }
        lessonDuration = 0;
        for (Program pr: programList) {
            if(!pr.getUnit()) {
                hasCardio = true;
            }

            lessonDuration += pr.getDuration();
        }
        lessonDuration *= 1000;

        createSurfaceView();
    }

    private void createSurfaceView() {
        mSurfaceView = (SurfaceView) findViewById(R.id.sv_colorize);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);
    }

    private void initializeControls() {
        mBttnControl = (FloatingActionButton) findViewById(R.id.bttn_control);
        mBttnControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isStopped = !isStopped;

                mBttnControl.setVisibility(View.INVISIBLE);
                mBttnClose.setVisibility(View.INVISIBLE);
                mBttnNext.setVisibility(View.INVISIBLE);
                mBttnBack.setVisibility(View.INVISIBLE);
            }
        });

        mBttnClose = (ImageView) findViewById(R.id.bttn_close_animation);
        mBttnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                Intent intent = new Intent(ColorizeActivity.this, MainActivity.class);
                intent.putExtra("return", -1);
                startActivity(intent);
            }
        });

        mBttnNext = (ImageView) findViewById(R.id.bttn_next);
        mBttnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count--;
                int id = sizeOfPrograms - count;
                setNextOrPreviousLesson(id);
            }
        });

        mBttnBack = (ImageView) findViewById(R.id.bttn_back);
        mBttnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                int id = sizeOfPrograms - count;
                setNextOrPreviousLesson(id);
            }
        });

        View mainView = findViewById(R.id.rl_animation);
        mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStopped = !isStopped;
                if (isStopped) {
                    mBttnControl.setVisibility(View.VISIBLE);
                    mBttnClose.setVisibility(View.VISIBLE);

                    if (count > 1)
                        mBttnNext.setVisibility(View.VISIBLE);

                    if (sizeOfPrograms - count > 0)
                        mBttnBack.setVisibility(View.VISIBLE);
                } else {
                    mBttnControl.setVisibility(View.INVISIBLE);
                    mBttnClose.setVisibility(View.INVISIBLE);
                    mBttnNext.setVisibility(View.INVISIBLE);
                    mBttnBack.setVisibility(View.INVISIBLE);
                }
            }
        });

        heartImage = (ImageView) findViewById(R.id.iv_heart);
        pulse = AnimationUtils.loadAnimation(ColorizeActivity.this, R.anim.heart_pulse);
        heartImage.startAnimation(pulse);
    }

    private void setNextOrPreviousLesson(int id) {
        curPosition = "Вдох";
        curDuration = programList.get(id).getBreath() * 1000;
        programDuration = programList.get(id).getDuration() * 1000;
        startTimer = 3000;
        startPosition = "Начинаем";

        mAngle = 0;
        mPartAngle = 36000 / (float)curDuration;

        setLessonDuration(id);

        mBttnControl.setVisibility(View.INVISIBLE);
        mBttnClose.setVisibility(View.INVISIBLE);
        mBttnNext.setVisibility(View.INVISIBLE);
        mBttnBack.setVisibility(View.INVISIBLE);

        isStopped = false;
    }

    private void setLessonDuration(int id) {
        lessonDuration = 0;
        for(int i = id; i < sizeOfPrograms; i++) {
            lessonDuration += programList.get(i).getDuration();
        }
        lessonDuration *= 1000;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        programDuration = programList.get(0).getDuration()*1000;
        curDuration = programList.get(0).getBreath()*1000;
        count = programList.size();
        mPartAngle = 36000 / (float)curDuration; // durationOfAllPrograms
        mAngle = 0;

        float heartTimes = (float) (lessonDuration / (60000 / userPulse));
        pulse.setInterpolator(new CycleInterpolator(heartTimes));
        pulse.startNow();
        pulse.setDuration((long) lessonDuration);

        startTimer = 3000;
        startPosition = "Начинаем";
        timer = new Timer("ColorizeScreen", true);
        timer.scheduleAtFixedRate(new ColorizeScreen(), 0, 100);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        timer.cancel();
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }

    private class ColorizeScreen extends TimerTask {

        @Override
        public void run() {
            if(!isStopped) {
                Canvas canvas = mSurfaceHolder.lockCanvas();

                Paint mClock = new Paint();
                mClock.setTextAlign(Paint.Align.CENTER);
                mClock.setTextSize(40);
                mClock.setColor(Color.BLACK);

                if(programDuration == 0 && count == 1) {
                    canvas.drawColor(Color.WHITE);
                    canvas.drawText("Заново", widthScreen / 2, heightScreen - 30, mClock);
                    mSurfaceHolder.unlockCanvasAndPost(canvas);
                    timer.cancel();
                    return;
                }
                else if(programDuration == 0) {
                    count--;
                    curPosition = "Вдох";
                    curDuration = programList.get(sizeOfPrograms - count).getBreath() * 1000;
                    programDuration = programList.get(sizeOfPrograms - count).getDuration() * 1000;
                    mAngle = 0;
                    mPartAngle = 36000 / (float)curDuration;

                    startTimer = 3000;
                    startPosition = "Следующее";
                }
                else if(curItem == 4) {
                    curPosition = "Вдох";
                    curItem = 0;
                }

                try {
                    // draw
                    mPaint = new Paint();
                    mArcPaint = new Paint();
                    mArcPaint.setColor(Color.rgb(227, 232, 232));
                    mArcPaint.setColorFilter(new ColorFilter());
                    mOval = new RectF(-200, -400, widthScreen + 200, heightScreen + 400);

                    canvas.drawColor(Color.WHITE);

                    if(startTimer != 0) {
                        mAngle = 0;
                        canvas.drawArc(mOval, 270, mAngle, true, mArcPaint);

                        int startTime = startTimer / 1000;
                        canvas.drawText(String.valueOf(startTime), widthScreen / 2, heightScreen / 2, mClock);
                        canvas.drawText(startPosition, widthScreen / 2, heightScreen - 30, mClock);
                        startTimer -= 100;
                    }
                    else {
                        if(curDuration <= 0) {
                            switch (curPosition) {
                                case "Вдох":
                                    curDuration = programList.get(sizeOfPrograms - count).getHoldFirst() * 1000;
                                    curPosition = "Задержка";
                                    break;
                                case "Выдох":
                                    curDuration = programList.get(sizeOfPrograms - count).getHoldSecond() * 1000;
                                    curPosition = "Задержка";
                                    break;
                                case "Задержка":
                                    curDuration = programList.get(sizeOfPrograms - count).getOut() * 1000;
                                    curPosition = "Выдох";
                                    break;
                            }

                            curItem++;
                            mAngle = 0;
                            mPartAngle = 36000 / (float) curDuration;
                        }
                        canvas.drawArc(mOval, 270, mAngle += mPartAngle, true, mArcPaint);

                        if(curDuration%1000 > 0 && curDuration%1000 <= 500) {
                            double curSec = (int)curDuration/1000 + 0.5;
                            canvas.drawText(String.valueOf(curSec), widthScreen / 2, heightScreen/2, mClock);
                        }
                        else if(curDuration%1000 > 500 && curDuration%1000 <= 900) {
                            int curSec = (int)curDuration/1000 + 1;
                            canvas.drawText(String.valueOf(curSec), widthScreen / 2, heightScreen/2, mClock);
                        }

                        canvas.drawText(curPosition, widthScreen / 2, heightScreen - 30, mClock);

                        canvas.drawText(String.valueOf((int)programDuration/1000), 30, heightScreen - 30, mClock);
                        canvas.drawText(String.valueOf((int)lessonDuration/1000), widthScreen - 30, heightScreen - 30, mClock);
                        programDuration -= 100;
                        curDuration -= 100;
                        lessonDuration -= 100;
                    }

                } finally {
                    if (canvas != null) {
                        mSurfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }
}
