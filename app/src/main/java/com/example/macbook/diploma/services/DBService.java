package com.example.macbook.diploma.services;

import android.content.Context;

import com.example.macbook.diploma.dataBase.DataBase;
import com.example.macbook.diploma.models.Lesson;
import com.example.macbook.diploma.models.Program;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by macbook on 10.12.15.
 */
public class DBService {
    private DataBase dataBase;
    private Context context;

    public DBService(Context context) {
        this.context = context;
    }

    public ArrayList<Lesson> getLessons() {
        openDB();
        assert dataBase != null;
        final ArrayList<Lesson> lessons = dataBase.getLessons();
        closeDB();

        return lessons;
    }

    public Lesson getLessonById(long id) {
        openDB();
        assert dataBase != null;
        Lesson lesson = dataBase.getLessonById(id);
        closeDB();

        return lesson;
    }

    public ArrayList<Program> getProgramsFromLesson(long id) {
        openDB();
        assert dataBase != null;
        ArrayList<Program> programs = dataBase.getProgramsFromLesson(id);
        closeDB();

        return programs;
    }

    public void updateLesson(Lesson lesson){
        openDB();
        dataBase.updateLesson(lesson);
        closeDB();
    }

    public long addLesson(Lesson lesson) {
        openDB();
        long id = dataBase.addLesson(lesson);
        closeDB();

        return id;
    }

    public Program getProgramById(long id) {
        openDB();
        assert dataBase != null;
        Program program = dataBase.getProgramById(id);
        closeDB();

        return program;
    }

    public void addProgram(Program program) {
        openDB();
        assert dataBase != null;
        dataBase.addProgram(program);
        closeDB();
    }

    public void updateProgram(Program program) {
        openDB();
        assert dataBase != null;
        dataBase.updateProgram(program);
        closeDB();
    }


    private void openDB() {
        dataBase = null;
        try {
            dataBase = new DataBase(context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void closeDB() {
        dataBase.close();
    }
}
